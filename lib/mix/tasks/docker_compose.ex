defmodule Mix.Tasks.DockerCompose do
  use Mix.Task
  require Logger

  @shortdoc "Run docker-compose commands"
  @moduledoc """
  Run docker-compose tasks with mix tasks.

  ## Examples

      # start postgres service in background
      mix docker_compose up -d postgres
  """
  @preferred_cli_env :dev

  defp cli_bin, do: "docker-compose"

  def run(args) do
    case docker_compose_installed?() do
      false ->
        Mix.raise """

        #{cli_bin()}: command not found

        Try install with instruction in https://docs.docker.com/compose/install/
        """
      true ->
        docker_compose(args)
    end
  end

  defp docker_compose(args) do
    Logger.debug "$ #{cli_bin()} #{args |> Enum.join(" ")}"
    system(cli_bin(), args, into: IO.stream(:stdio, :line))
  end

  defp system(cmd, args, opts \\ []) do
    System.cmd(cmd, args, opts)
  end

  defp docker_compose_installed? do
    try do
      case system(cli_bin(), ["--version"]) do
        {_, 0} -> true
        {_, _} -> false
      end
    rescue
      err in [ErlangError] ->
        type = err |> Map.get(:original)

        case type in [:enoent, :eacces] do
          true -> false
          _ ->
            Exception.message(err)
            false
        end
    end
  end

end
