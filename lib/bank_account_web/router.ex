defmodule BankAccountWeb.Router do
  use BankAccountWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BankAccountWeb do
    pipe_through :api

    resources "/accounts"    , AccountController    , except: [:new, :edit]
    resources "/transactions", TransactionController, only: [:index, :show, :create]
  end
end
