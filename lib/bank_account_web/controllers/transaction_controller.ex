defmodule BankAccountWeb.TransactionController do
  use BankAccountWeb, :controller

  alias BankAccount.Repo
  alias BankAccount.Bank
  alias BankAccount.Bank.Transaction

  action_fallback BankAccountWeb.FallbackController

  def index(conn, _params) do
    transactions = Bank.list_transactions()
    render(conn, "index.json", transactions: transactions)
  end

  def create(conn, %{"transaction" => transaction_params}) do
    with  {:ok, %Transaction{} = transaction} <- Bank.create_transaction(transaction_params),
          transaction <- transaction |> Repo.preload([:source_account, :destination_account])
    do
      conn
      |> put_status(:created)
      |> put_resp_header("location", transaction_path(conn, :show, transaction))
      |> render("show.json", transaction: transaction)
    end
  end

  def show(conn, %{"id" => id}) do
    transaction = Bank.get_transaction!(id)
    render(conn, "show.json", transaction: transaction)
  end

end
