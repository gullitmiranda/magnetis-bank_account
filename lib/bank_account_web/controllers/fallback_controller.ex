defmodule BankAccountWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use BankAccountWeb, :controller

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(BankAccountWeb.ErrorView, :"404")
  end

  def call(conn, {:error, changeset}) do
    changeset = changeset |> BankAccountWeb.ErrorHelpers.error_to_changeset()
    conn
    |> put_status(:unprocessable_entity)
    |> render(BankAccountWeb.ChangesetView, "error.json", changeset: changeset)
  end

end
