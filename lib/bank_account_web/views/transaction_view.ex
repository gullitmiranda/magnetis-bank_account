defmodule BankAccountWeb.TransactionView do
  use BankAccountWeb, :view
  alias __MODULE__

  alias BankAccount.Bank.Account
  alias BankAccountWeb.AccountView

  def render("index.json", %{
    transactions: transactions,
    conn: %{params: %{"account_id" => account_id}}
  } = _assigns) do
    {_, current_balance} = parse_amounts(transactions |> hd(), account_id)

    {_, data} = transactions
      |> Enum.reduce({current_balance, []}, fn(transaction, {balance, data}) ->
        %{inserted_at: inserted_at} = transaction
        {transaction_amount, _} = parse_amounts(transaction, account_id)
        line = [{inserted_at, %{amount: transaction_amount, balance: balance}}] |> Map.new
        {balance - transaction_amount, data ++ [line]}
      end)

    %{data: data}
  end

  def render("index.json", %{transactions: transactions}) do
    %{data: render_many(transactions, TransactionView, "transaction.json")}
  end

  def render("show.json", %{transaction: transaction}) do
    %{data: render_one(transaction, TransactionView, "transaction.json")}
  end

  def render("transaction.json", %{transaction: transaction}) do
    transaction
    |> Map.take(~w(id amount inserted_at source_account_id destination_account_id)a)
    |> maybe_render_account(transaction, :source_account)
    |> maybe_render_account(transaction, :destination_account)
  end

  defp maybe_render_account(data, transaction, assoc_key) do
    case transaction |> Map.get(assoc_key) do
      %Ecto.Association.NotLoaded{} -> data
      nil -> data
      account ->
        data
        |> Map.put(assoc_key, AccountView.render("account.json", account: account))
    end
  end

  defp parse_amounts(%{
      source_account:      %Account{} = source_account,
      destination_account: %Account{} = destination_account,
      amount: amount,
    }, account_id) do
    case "#{source_account.id}" == "#{account_id}" do
      false -> {amount, destination_account.amount}
      true  -> {amount * -1, source_account.amount}
    end
  end
end
