defmodule BankAccountWeb.AccountView do
  use BankAccountWeb, :view
  alias BankAccountWeb.AccountView

  def render("index.json", %{accounts: accounts}) do
    %{data: render_many(accounts, AccountView, "account.json")}
  end

  def render("show.json", %{account: account}) do
    %{data: render_one(account, AccountView, "account.json")}
  end

  def render("account.json", %{account: account}) do
    %{id: account.id,
      amount: account.amount}
  end
end
