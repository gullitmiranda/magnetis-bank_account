defmodule BankAccount.Repo do
  use Ecto.Repo, otp_app: :bank_account

  @doc """
  Dynamically loads the repository url from the
  DATABASE_URL environment variable.
  """
  def init(_, opts) do
    {:ok, Keyword.put(opts, :url, database_url())}
  end

  defp database_url do
    case System.get_env("DATABASE_URL") do
      nil -> "ecto+postgres://magnetis:magnetis@localhost:54329/bank_account_#{Mix.env}"
      url -> url
    end
  end
end
