defmodule BankAccount.Bank.Account do
  use Ecto.Schema
  import Ecto.Changeset
  alias BankAccount.Bank.Account


  schema "accounts" do
    field :amount, :float

    timestamps()
  end

  @doc false
  def changeset(%Account{} = account, attrs) do
    account
    |> cast(attrs, [:amount])
    |> validate_required([:amount])
  end
end
