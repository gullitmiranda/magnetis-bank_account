defmodule BankAccount.Bank.Transaction do
  use Ecto.Schema
  import Ecto.Changeset

  alias BankAccount.Bank
  alias BankAccount.Bank.{
    Account,
    Transaction,
  }

  schema "transactions" do
    field :amount, :float
    belongs_to :source_account     , Account
    belongs_to :destination_account, Account

    timestamps()
  end

  @doc false
  @required ~w(source_account_id destination_account_id amount)a
  def changeset(%Transaction{} = transaction, attrs) do
    transaction
    |> cast(attrs, @required)
    |> validate_required(@required)
    |> validate_number(:amount, greater_than: 0)
    |> assoc_constraint(:source_account)
    |> assoc_constraint(:destination_account)
    |> prepare_changes(fn changeset ->
      amount = changeset |> get_field(:amount)
      with  source_account              <- changeset |> get_field(:source_account_id     ) |> Bank.get_account!(),
            destination_account         <- changeset |> get_field(:destination_account_id) |> Bank.get_account!(),
            :ok                         <- validate_transaction_amount(source_account, amount),
            {:ok, _source_account}      <- Bank.update_account(source_account     , %{amount: source_account.amount - amount}),
            {:ok, _destination_account} <- Bank.update_account(destination_account, %{amount: destination_account.amount + amount})
        do
          changeset
      else
        {:error, reason} -> changeset.repo.rollback(reason)
      end
    end)
  end

  defp validate_transaction_amount(%{amount: source_amount}, amount) do
    case (source_amount - amount) >= 0 do
      true -> :ok
      false -> {:error, {:amount, "insufficient funds in source account"}}
    end
  end

end
