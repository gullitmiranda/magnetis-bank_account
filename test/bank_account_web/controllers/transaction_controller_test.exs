defmodule BankAccountWeb.TransactionControllerTest do
  use BankAccountWeb.ConnCase

  alias BankAccount.Bank

  @create_attrs %{amount: 120.5}
  @invalid_attrs %{amount: nil}

  def fixture(:transaction) do
    {:ok, transaction} = Bank.create_transaction(@create_attrs)
    transaction
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all transactions", %{conn: conn} do
      conn = get conn, transaction_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end

    test "lists all transactions from one account", %{conn: conn} do
      {:ok, account1} = Bank.create_account(%{amount: 200})
      {:ok, account2} = Bank.create_account(%{amount: 200})

      attrs_1to2 = %{source_account_id: account1.id, destination_account_id: account2.id}
      attrs_2to1 = %{source_account_id: account2.id, destination_account_id: account1.id}

      {:ok, %{inserted_at: inserted_at1}} = Bank.create_transaction(attrs_1to2 |> Map.put(:amount, 10))
      {:ok, %{inserted_at: inserted_at2}} = Bank.create_transaction(attrs_1to2 |> Map.put(:amount, 20))
      {:ok, %{inserted_at: inserted_at3}} = Bank.create_transaction(attrs_1to2 |> Map.put(:amount, 0.50))
      {:ok, %{inserted_at: inserted_at4}} = Bank.create_transaction(attrs_2to1 |> Map.put(:amount, 100.50))

      conn = get(conn, transaction_path(conn, :index, account_id: account1.id))
      assert %{"data" => data} = json_response(conn, 200)

      assert data == [
        %{"#{inserted_at4}" => %{"amount" => 100.5, "balance" => 270.0}},
        %{"#{inserted_at3}" => %{"amount" => -0.5 , "balance" => 169.5}},
        %{"#{inserted_at2}" => %{"amount" => -20.0, "balance" => 170.0}},
        %{"#{inserted_at1}" => %{"amount" => -10.0, "balance" => 190.0}},
      ]
    end
  end

  describe "create transaction" do
    test "should return transaction when have money", %{conn: conn} do
      {:ok, source_account}      = Bank.create_account(%{amount: 200})
      {:ok, destination_account} = Bank.create_account(%{amount: 200})

      source_account_id = source_account.id
      destination_account_id = destination_account.id

      params = %{
        source_account_id: source_account_id,
        destination_account_id: destination_account_id,
        amount: 100,
      }

      conn = post(conn, transaction_path(conn, :create), transaction: params)

      assert %{"data" => data} = json_response(conn, 201)

      assert data["source_account"     ]["amount"] == 100
      assert data["destination_account"]["amount"] == 300

      assert %{"id" => id} = data

      conn = get(conn, transaction_path(conn, :show, id))
      assert %{"data" => data} = json_response(conn, 200)

      assert match?(%{
        "id" => ^id,
        "amount" => 100.0,
        "source_account_id" => ^source_account_id,
        "destination_account_id" => ^destination_account_id,
        "inserted_at" => _,
      }, data)
    end

    test "renders errors when amount is negative", %{conn: conn} do
      {:ok, source_account}      = Bank.create_account(%{amount: 100})
      {:ok, destination_account} = Bank.create_account(%{amount: 0})

      params = %{
        source_account_id: source_account.id,
        destination_account_id: destination_account.id,
        amount: -200,
      }

      conn = post(conn, transaction_path(conn, :create), transaction: params)

      assert %{"errors" => errors} = json_response(conn, 422)
      assert errors

      errors = errors |> Map.to_list()

      assert {"amount", ["must be greater than 0"]} in errors
      assert errors |> Enum.count == 1
    end

    test "renders errors when the source account does not have sufficient funds", %{conn: conn} do
      {:ok, source_account}      = Bank.create_account(%{amount: 100})
      {:ok, destination_account} = Bank.create_account(%{amount: 0})

      params = %{
        source_account_id: source_account.id,
        destination_account_id: destination_account.id,
        amount: 200,
      }

      conn = post(conn, transaction_path(conn, :create), transaction: params)

      assert %{"errors" => errors} = json_response(conn, 422)
      assert errors

      errors = errors |> Map.to_list()

      assert {"amount", ["insufficient funds in source account"]} in errors
      assert errors |> Enum.count == 1
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, transaction_path(conn, :create), transaction: @invalid_attrs)
      assert %{"errors" => errors} = json_response(conn, 422)
      assert errors

      errors = errors |> Map.to_list()

      assert {"source_account_id"     , ["can't be blank"]} in errors
      assert {"destination_account_id", ["can't be blank"]} in errors
      assert {"amount"                , ["can't be blank"]} in errors
      assert errors |> Enum.count == 3
    end
  end

end
