defmodule BankAccount.BankTest do
  use BankAccount.DataCase

  alias BankAccount.Bank
  alias BankAccount.Bank.Account

  describe "accounts" do
    @valid_attrs %{amount: 120.5}
    @update_attrs %{amount: 456.7}
    @invalid_attrs %{amount: nil}

    def account_fixture(attrs \\ %{}) do
      {:ok, account} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Bank.create_account()

      account
    end

    test "list_accounts/0 returns all accounts" do
      account = account_fixture()
      assert Bank.list_accounts() == [account]
    end

    test "get_account!/1 returns the account with given id" do
      account = account_fixture()
      assert Bank.get_account!(account.id) == account
    end

    test "create_account/1 with valid data creates a account" do
      assert {:ok, %Account{} = account} = Bank.create_account(@valid_attrs)
      assert account.amount == 120.5
    end

    test "create_account/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Bank.create_account(@invalid_attrs)
    end

    test "update_account/2 with valid data updates the account" do
      account = account_fixture()
      assert {:ok, account} = Bank.update_account(account, @update_attrs)
      assert %Account{} = account
      assert account.amount == 456.7
    end

    test "update_account/2 with invalid data returns error changeset" do
      account = account_fixture()
      assert {:error, %Ecto.Changeset{}} = Bank.update_account(account, @invalid_attrs)
      assert account == Bank.get_account!(account.id)
    end

    test "delete_account/1 deletes the account" do
      account = account_fixture()
      assert {:ok, %Account{}} = Bank.delete_account(account)
      assert_raise Ecto.NoResultsError, fn -> Bank.get_account!(account.id) end
    end

    test "change_account/1 returns a account changeset" do
      account = account_fixture()
      assert %Ecto.Changeset{} = Bank.change_account(account)
    end
  end

  describe "transactions" do
    @valid_attrs %{amount: 120.5}

    def transaction_fixture(attrs \\ %{}) do
      attrs =
        case attrs do
          %{source_account_id: _} -> attrs
          _ ->
            {:ok, source_account} = Bank.create_account(%{amount: 200})
            attrs
            |> Map.put(:source_account_id, source_account.id)
        end

      attrs =
        case attrs do
          %{destination_account_id: _} -> attrs
          _ ->
            {:ok, destination_account} = Bank.create_account(%{amount: 200})
            attrs
            |> Map.put(:destination_account_id, destination_account.id)
        end

      {:ok, transaction} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Bank.create_transaction()

      transaction
    end

    test "list_transactions/0 returns all transactions" do
      transaction = transaction_fixture()
        |> Repo.preload([:source_account, :destination_account])
      assert Bank.list_transactions() == [transaction]
    end

    test "get_transaction!/1 returns the transaction with given id" do
      transaction = transaction_fixture()
      assert Bank.get_transaction!(transaction.id) == transaction
    end

    test "create_transaction/1 with valid data creates a transaction" do
      {:ok, source_account}      = Bank.create_account(%{amount: 200})
      {:ok, destination_account} = Bank.create_account(%{amount: 200})

      params = %{
        "source_account_id" => source_account.id,
        "destination_account_id" => destination_account.id,
        "amount" => 200,
      }

      assert {:ok, transaction} = Bank.create_transaction(params)

      transaction = transaction |> Repo.preload([:source_account, :destination_account])

      assert transaction.source_account.amount == 0
      assert transaction.destination_account.amount == 400

      assert Repo.get(Account, source_account.id).amount == 0
      assert Repo.get(Account, destination_account.id).amount == 400
    end

    test "create_transaction/1 with source account with insufficient funds" do
      {:ok, source_account}      = Bank.create_account(%{amount: 10})
      {:ok, destination_account} = Bank.create_account(%{amount: 200})

      params = %{
        "source_account_id" => source_account.id,
        "destination_account_id" => destination_account.id,
        "amount" => 200,
      }

      assert {:error, {:amount, "insufficient funds in source account"}} == Bank.create_transaction(params)

      assert Repo.get(Account, source_account.id).amount == 10
      assert Repo.get(Account, destination_account.id).amount == 200
    end

    test "create_transaction/1 with source account with negative amount" do
      {:ok, source_account}      = Bank.create_account(%{amount: 100})
      {:ok, destination_account} = Bank.create_account(%{amount: 200})

      params = %{
        "source_account_id" => source_account.id,
        "destination_account_id" => destination_account.id,
        "amount" => -10,
      }

      assert {:error, changeset} = Bank.create_transaction(params)

      %{errors: errors} = changeset
      assert {:amount, {"must be greater than %{number}", [validation: :number, number: 0]}} in errors
      assert errors |> Enum.count == 1

      assert Repo.get(Account, source_account.id).amount == 100
      assert Repo.get(Account, destination_account.id).amount == 200
    end

    test "create_transaction/1 with empty params returns error" do
      assert {:error, %Ecto.Changeset{errors: errors}} = Bank.create_transaction(%{})
      assert {:source_account_id     , {"can't be blank", [validation: :required]}} in errors
      assert {:destination_account_id, {"can't be blank", [validation: :required]}} in errors
      assert {:amount                , {"can't be blank", [validation: :required]}} in errors
      assert errors |> Enum.count == 3
      assert Account |> Repo.all == []
    end
  end
end
