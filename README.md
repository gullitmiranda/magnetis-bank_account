# BankAccount

## Requirements

- Elixir `~> 1.4` and Erlang OTP 20: <https://elixir-lang.org/install.html>
- Docker Compose `~> 1.14`: <https://docs.docker.com/compose/install/>

## Start application

```sh
# Run provision and start application
mix deps.install
# Setup database and run migrations
mix ecto.setup
# run database seeds to create default accounts
mix ecto.seeds
# Start Phoenix endpoint with
mix phx.server
```

Now you can visit [`http://localhost:4000`](http://localhost:4000) from your browser.

## Run tests

```sh
# Run provision and start application
MIX_ENV=test mix deps.install
# Setup database and run migrations
MIX_ENV=test mix ecto.setup
# Run all tests
MIX_ENV=test mix test
# Runs only tests which reference modules that changed since the last test
MIX_ENV=test mix test --stale
```


## Requests Samples

#### Get list of accounts:

```sh
$ curl http://localhost:4000/accounts
{"data":[{"id":1,"amount":200.0},{"id":2,"amount":200.0}]}
```

#### Get account infos (with amount):

```sh
$ curl http://localhost:4000/accounts/1
{"data":{"id":1,"amount":200.0}}
```

#### Make a new transference:

```sh
$ curl -X POST http://localhost:4000/transactions \
  -H "Content-Type: application/json" \
  -d '{"transaction": {"source_account_id": 1, "destination_account_id": 2, "amount": 10}}'
{"data":{"source_account":{"id":1,"amount":190.0},"destination_account":{"id":2,"amount":210.0},"amount":10}}
```

#### Get transactions by account_id:

```sh
$ curl http://localhost:4000/transactions\?account_id=1
{"data":[{"2017-09-19 21:04:01.386744":{"balance":309.5,"amount":-0.5}},{"2017-09-19 21:03:26.591341":{"balance":310.0,"amount":0.5}},{"2017-09-19 21:03:10.524351":{"balance":309.5,"amount":50.0}},{"2017-09-19 20:43:50.951167":{"balance":259.5,"amount":50.0}},{"2017-09-19 20:34:49.322717":{"balance":209.5,"amount":-10.0}}]}
```

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
