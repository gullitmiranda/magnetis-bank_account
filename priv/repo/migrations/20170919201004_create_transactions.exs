defmodule BankAccount.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions) do
      add :amount, :float
      add :source_account_id, references(:accounts, on_delete: :nothing)
      add :destination_account_id, references(:accounts, on_delete: :nothing)

      timestamps()
    end

    create index(:transactions, [:source_account_id])
    create index(:transactions, [:destination_account_id])
  end
end
