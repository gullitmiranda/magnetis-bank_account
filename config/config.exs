# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :bank_account,
  ecto_repos: [BankAccount.Repo],
  generators: [binary_id: false]

# Configures the endpoint
config :bank_account, BankAccountWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "zg3j1c+1gUwRuH/79vfOAZxgLxqdFxgQOQktQ64mZiNbTerObpHbitQ++zBzQNa4",
  render_errors: [view: BankAccountWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: BankAccount.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure your database
config :bank_account, BankAccount.Repo,
  adapter: Ecto.Adapters.Postgres

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
