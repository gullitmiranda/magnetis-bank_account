defmodule BankAccount.Mixfile do
  use Mix.Project

  def project do
    [
      app: :bank_account,
      version: "0.0.1",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env),
      compilers: [:phoenix, :gettext] ++ Mix.compilers,
      start_permanent: Mix.env == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {BankAccount.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.3.0"},
      {:phoenix_pubsub, "~> 1.0"},
      {:phoenix_ecto, "~> 3.2"},
      {:postgrex, ">= 0.0.0"},
      {:gettext, "~> 0.11"},
      {:cowboy, "~> 1.0"},
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "deps.install":   ["deps.get", "deps.compile"],
      "test":           ["ecto.reset" , "test"],

      # Ecto (Database)
      "__ecto.setup": ["ecto.create", "ecto.migrate"],
      "ecto.setup": ["services.start", "__ecto.setup"],
      "ecto.reset": ["services.start", "ecto.drop", "__ecto.setup"],
      "ecto.seeds": ["run priv/repo/seeds.exs"],

      # Services (using docker-compose.yml)
      "services.start": [&services_start/1],
      "services.stop":  [&services_stop/1],
    ]
  end

  @services ~s(postgres)
  defp services_start(_) do
    Mix.shell.info "Starting #{@services} services"
    Mix.Task.run("docker_compose", ~w(up -d #{@services}))
  end

  defp services_stop(_) do
    Mix.shell.info "Stoping #{@services} services"
    Mix.Task.run("docker_compose", ~w(down))
  end
end
